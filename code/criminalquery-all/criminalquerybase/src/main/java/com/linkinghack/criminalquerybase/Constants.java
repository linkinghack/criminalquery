package com.linkinghack.criminalquerybase;

public class Constants {
    public static Integer UserRoleManeger = 1;
    public static Integer UserRoleNormal = 0;

    public static Integer SexUnknown = 0;
    public static Integer SexMale = 1;
    public static Integer SexFemale = 2;

    public static Integer DefaultPageSize = 10;
}
